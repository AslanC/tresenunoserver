"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
const fs_extra_1 = __importDefault(require("fs-extra"));
const document_1 = require("../libs/document");
//importando modelo file
const file_1 = __importDefault(require("../models/file"));
//import multer, { diskStorage } from 'multer';
const mongoose_1 = require("mongoose");
async function getFiles(req, res) {
    const Jsonids = req.headers.ids !== undefined ? req.headers.ids : '';
    const ids = Jsonids.toString().split(',') || [''];
    let json = [null];
    try {
        for (let i = 0; i < ids.length; i++) {
            const element = ids[i];
            if (!(mongoose_1.Types.ObjectId.isValid(element))) {
                return res.json({
                    'error': 'uno de los ids no es valido'
                });
            }
            if (i === 0) {
                json[0] = (await file_1.default.findById(element));
            }
            else {
                json[i] = (await file_1.default.findById(element));
            }
        }
    }
    catch (error) {
        console.log(error);
    }
    return res.json(json);
}
exports.getFiles = getFiles;
async function getFilesPage(req, res) {
    const { page } = req.params;
    let files;
    if (page != undefined) {
        files = await file_1.default.find({ 'page': page });
    }
    return res.json(files);
}
exports.getFilesPage = getFilesPage;
async function postFile(req, res) {
    const d = new Date();
    let { page } = req.body;
    let response;
    const fs = req.files;
    response = await funResponse(fs, page, d);
    return res.send(response);
}
exports.postFile = postFile;
async function deleteFiles(req, res) {
    const Jsonids = req.body;
    const ids = Jsonids.id;
    let json = [null];
    let rJson = {};
    try {
        for (let i = 0; i < ids.length; i++) {
            const element = ids[i];
            const fl = await file_1.default.findByIdAndRemove(element);
            if (fl) {
                fs_extra_1.default.unlink(path_1.default.resolve(fl.filePath))
                    .then(() => {
                    console.log('se ha removido el archivo: ');
                })
                    .catch(err => console.log('No se pudo remover el archivo ', err));
            }
            i === 0 ? json[0] = fl : json.push(fl);
        }
    }
    catch (error) {
        console.log(error);
        rJson = {
            'message': '',
            'error': 'Hubo un error al borrar la foto'
        };
    }
    return res.json(json[0] != null ? json : rJson);
}
exports.deleteFiles = deleteFiles;
async function updateFile(req, res) {
    const Jsonids = req.body;
    const { title, page } = Jsonids;
    let rJson = {};
    let updatedFile = null;
    try {
        updatedFile = await file_1.default.findByIdAndUpdate(Jsonids.id[0], {
            title,
            page
        }, { new: true }); //new: true, trae el nuevo dato actualizado
        rJson = {
            'message': 'Se ha actualizado correctamente',
            'error': '',
            updatedFile
        };
    }
    catch (error) {
        console.log(error);
        rJson = {
            'message': '',
            'error': 'Hubo un error al actualizar'
        };
    }
    return res.json(rJson);
}
exports.updateFile = updateFile;
async function funResponse(fs, page, d) {
    let firstDocument = true;
    let response = [{}];
    return new Promise(async (resolve, reject) => {
        for (let i = 0; i < fs.length; i++) {
            const element = fs[i];
            const document = await document_1.createDocument(element, page, d, true);
            firstDocument ? (response[0] = await document_1.saveDocument(document), firstDocument = false) : response.push(await document_1.saveDocument(document));
        }
        resolve(response);
    });
}
