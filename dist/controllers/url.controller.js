"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const document_1 = require("../libs/document");
const file_1 = __importDefault(require("../models/file"));
async function postUrlFile(req, res) {
    const d = new Date();
    let files = req.body.urls;
    let response;
    response = await funResponse(files, d);
    return res.send(response);
}
exports.postUrlFile = postUrlFile;
async function deleteUrlFiles(req, res) {
    const Jsonids = req.body;
    const ids = Jsonids.id;
    let json = [null];
    let rJson = {};
    try {
        for (let i = 0; i < ids.length; i++) {
            const element = ids[i];
            const fl = await file_1.default.findByIdAndRemove(element);
            i === 0 ? json[0] = fl : json.push(fl);
        }
    }
    catch (error) {
        console.log(error);
        rJson = {
            'message': '',
            'error': 'Hubo un error al borrar el archivo'
        };
    }
    return res.json(json[0] != null ? json : rJson);
}
exports.deleteUrlFiles = deleteUrlFiles;
async function updateUrlFile(req, res) {
    const Jsonids = req.body.urls;
    let updatedFile = null;
    let rJson = {};
    try {
        for (let i = 0; i < Jsonids.length; i++) {
            const element = Jsonids[i];
            const { title, page, filePath, type } = element;
            updatedFile = await file_1.default.findByIdAndUpdate(element.id, {
                title,
                page,
                filePath,
                type
            }, { new: true }); //new: true, trae el nuevo dato actualizado
            rJson = {
                'message': 'Se ha actualizado correctamente',
                'error': '',
                updatedFile
            };
        }
    }
    catch (error) {
        console.log(error);
        rJson = {
            'message': '',
            'error': 'Hubo un error al actualizar'
        };
    }
    return res.json(rJson);
}
exports.updateUrlFile = updateUrlFile;
async function funResponse(files, d) {
    let firstdocument = true;
    const response = [{}];
    return new Promise(async (resolve, reject) => {
        for (let i = 0; i < files.length; i++) {
            const element = files[i];
            const document = await document_1.createDocument(element, element.page, d, false);
            firstdocument ? (response[0] = await document_1.saveDocument(document), firstdocument = false) : response.push(await document_1.saveDocument(document));
        }
        resolve(response);
    });
}
