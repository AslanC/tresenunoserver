"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const path_1 = __importDefault(require("path"));
const cors_1 = __importDefault(require("cors"));
//morgan es un middleware que me permite ver las peticiones que hace el cliente por consola
const morgan_1 = __importDefault(require("morgan"));
const app = express_1.default();
const index_1 = __importDefault(require("./routes/index"));
//settings
app.set('port', process.env.PORT || 8000);
//middlewares
//app usa el modulo morgan en la versión de desarrollo
app.use(morgan_1.default('dev'));
//entender los datos que me envian con estructura Json
app.use(express_1.default.json());
//cors options
const corsOptions = {
    "origin": "http://localhost:4200",
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE"
};
app.use(cors_1.default(corsOptions));
//routes
app.use('/api', index_1.default);
//esta carpeta se utiliza para almacenar archivos publicos
app.use('./uploads', express_1.default.static(path_1.default.resolve('uploads')));
//la funcion path.resolve(string) devuleve toda la ruta desde el inicio de una carpeta, ejemplo: ../c/user/unit/folfer/folder/.../uploads
//para no escribir todo esto, path.resolve(string) nos devuelve esa url
exports.default = app;
