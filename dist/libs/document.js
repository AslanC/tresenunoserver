"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const file_1 = __importDefault(require("../models/file"));
const path_1 = __importDefault(require("path"));
async function createDocument(document, page, d, ispath) {
    return new Promise((resolve, reject) => {
        const newFile = {
            title: document.filename,
            date: `${d.getFullYear()}/${d.getMonth() < 10 ? '0' + d.getMonth() : d.getMonth()}/${d.getDate() < 10 ? 0 + d.getDate() : d.getDate()}-${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`,
            filePath: document.path,
            type: ispath ? path_1.default.extname(document.path).split('.')[1] : document.type,
            page: page
        };
        resolve(newFile);
    });
}
exports.createDocument = createDocument;
// function createDocument(docuemnte: {}, page: string, d:Date): Promise<{}>{
// }
async function saveDocument(newFile) {
    const fl = new file_1.default(newFile);
    let json;
    try {
        await fl.save();
        json = ({
            message: 'la ruta del archivo se guardó con exito en la base de datos',
            error: '',
            fl
        });
    }
    catch (err) {
        console.log(err);
        json = ({
            message: '',
            error: 'Hubo un error al guardar',
        });
    }
    return json;
}
exports.saveDocument = saveDocument;
