"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const schema = new mongoose_1.Schema({
    title: String,
    date: String,
    filePath: String,
    type: String,
    page: String,
});
//create colecction file(s) into mongodb. en plural
exports.default = mongoose_1.model('file', schema);
