"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
async function starConnection() {
    mongoose_1.connect('mongodb://localhost/booksdb', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    }).catch(err => {
        console.log('error en conexion: ' + err);
    });
    console.log('database is connected');
}
exports.starConnection = starConnection;
