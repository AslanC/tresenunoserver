"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = express_1.Router();
//controllers
const file_controller_1 = require("../controllers/file.controller");
const url_controller_1 = require("../controllers/url.controller");
//libs
const multer_1 = __importDefault(require("../libs/multer"));
router.route('/files')
    //file, es el fieldnmae que trae el request
    .post(multer_1.default.array('file', 15), file_controller_1.postFile)
    .get(file_controller_1.getFiles)
    .delete(file_controller_1.deleteFiles)
    .put(file_controller_1.updateFile);
router.route('/:page')
    .get(file_controller_1.getFilesPage);
router.route('/uploadppt')
    .post(url_controller_1.postUrlFile)
    .put(url_controller_1.updateUrlFile)
    .delete(url_controller_1.deleteUrlFiles);
exports.default = router;
