import { Request, Response } from 'express';
import { createDocument, saveDocument } from '../libs/document';

import file from '../models/file';
import { Document } from 'mongoose';

export async function postUrlFile(req: Request, res: Response): Promise<Response> {
    const d = new Date();
    let files: [] = req.body.urls;
    let response: [{}];

    response = await funResponse(files, d);

    return res.send(response);
}

export async function deleteUrlFiles(req: Request, res: Response) : Promise<Response> {
    const Jsonids = req.body;
    const ids: [] = Jsonids.id;
    let json: [Document | null] = [null];
    let rJson:{} = {};
    try {
        for (let i = 0; i < ids.length; i++) {
            const element = ids[i];            
            const fl = await file.findByIdAndRemove(element);
            
            i === 0 ? json[0] = fl : json.push(fl);
        }
    } catch (error) {
        console.log(error);
        rJson = {
            'message': '',
            'error': 'Hubo un error al borrar el archivo'
        };
    }

    return res.json(json[0] != null ? json: rJson);
}

export async function updateUrlFile(req: Request, res: Response) : Promise<Response>{
    const Jsonids = req.body.urls;
    let updatedFile: Document | null = null;
    let rJson:{} = {};
    try {
        for (let i = 0; i < Jsonids.length; i++) {
            const element = Jsonids[i];
            const {title, page, filePath, type} = element;
            updatedFile = await file.findByIdAndUpdate(element.id, {
                title,
                page,
                filePath,
                type
            }, {new: true}); //new: true, trae el nuevo dato actualizado
            rJson = {
                'message': 'Se ha actualizado correctamente',
                'error': '',
                updatedFile
            };   
        }
    } catch (error) {
        console.log(error);
        rJson = {
            'message': '',
            'error': 'Hubo un error al actualizar'
        };
    }

    return res.json(rJson);
    
}

async function funResponse(files: [], d: Date): Promise<[{}]>{
    let firstdocument: boolean = true;
    const response:[{}] = [{}];

    return new Promise(async(resolve, reject) => {
        for (let i = 0; i < files.length; i++) {
            const element: any = files[i];
            
            const document = await createDocument(element, element.page, d, false);

            firstdocument ?(response[0] = await saveDocument(document), firstdocument=false) :response.push(await saveDocument(document));
        }

        resolve(response);
    });

}
