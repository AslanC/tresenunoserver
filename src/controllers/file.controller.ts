import {Request, Response} from 'express';
import path from 'path';
import fs from 'fs-extra';
import {createDocument, saveDocument} from '../libs/document';

//importando modelo file
import file from '../models/file';
//import multer, { diskStorage } from 'multer';
import { Document, Mongoose, Types } from 'mongoose';

export async function getFiles(req: Request, res:Response): Promise<Response> {
    const Jsonids = req.headers.ids !== undefined ? req.headers.ids : '';
    const ids: string[] = Jsonids.toString().split(',') || [''];
    let json: [Document | null] = [null];

    try {
        for (let i = 0; i < ids.length; i++) {
            const element = ids[i];

            if (!(Types.ObjectId.isValid(element))){
                return res.json({
                    'error': 'uno de los ids no es valido'
                });
            }

            if(i === 0){
                json[0] = (await file.findById(element));

            }else{
                json[i] = (await file.findById(element));
            }
        }

    } catch (error) {
        console.log(error);       
    }

    return res.json(json);
}

export async function getFilesPage(req: Request, res:Response): Promise<Response> {
    const { page } = req.params;
    let files;
    if(page != undefined){
        files = await file.find({'page': page});
    }

    return res.json(files);
}

export async function postFile(req: Request, res: Response): Promise<Response> {
    const d = new Date();
    let {page} = req.body;
    let response: [{}];
    
    const fs: Express.Multer.File[] = req.files as [];

    response = await funResponse(fs, page, d);

    return res.send(response);
}

export async function deleteFiles(req: Request, res: Response) : Promise<Response> {
    const Jsonids = req.body;
    const ids: [] = Jsonids.id;
    let json: [Document | null] = [null];
    let rJson:{} = {};
    try {
        for (let i = 0; i < ids.length; i++) {
            const element = ids[i];            
            const fl = await file.findByIdAndRemove(element);
            if(fl){
                fs.unlink(path.resolve(fl.filePath))
                .then(() =>{
                     console.log('se ha removido el archivo: ',);
                })
                .catch(err => console.log('No se pudo remover el archivo ', err));
            }
            i === 0 ? json[0] = fl : json.push(fl);
        }
    } catch (error) {
        console.log(error);
        rJson = {
            'message': '',
            'error': 'Hubo un error al borrar la foto'
        };
    }

    return res.json(json[0] != null ? json: rJson);
}

export async function updateFile(req: Request, res: Response) : Promise<Response>{
    const Jsonids = req.body;
    const {title, page} = Jsonids;
    let rJson:{} = {};
    
    let updatedFile: Document | null = null;
    try {
        updatedFile = await file.findByIdAndUpdate(Jsonids.id[0], {
            title,
            page
        }, {new: true}); //new: true, trae el nuevo dato actualizado
        rJson = {
            'message': 'Se ha actualizado correctamente',
            'error': '',
            updatedFile
        };
    } catch (error) {
        console.log(error);
        rJson = {
            'message': '',
            'error': 'Hubo un error al actualizar'
        };
    }

    return res.json(rJson);
    
}

async function funResponse(fs: Express.Multer.File[], page:string , d:Date): Promise<[{}]>{
    
    let firstDocument : boolean = true;
    let response: [{}] = [{}];

    return new Promise(async(resolve, reject) => {
        for (let i = 0; i < fs.length; i++) {
            const element = fs[i];

            const document = await createDocument(element,page,d, true);

            firstDocument ? (response[0] = await saveDocument(document),firstDocument = false) : response.push(await saveDocument(document));
        }

        resolve(response)
    });
}
