import expres from 'express';
import path from 'path';
import cors from 'cors';
//morgan es un middleware que me permite ver las peticiones que hace el cliente por consola
import morgan from 'morgan';

const app = expres();

import indexRoutes from './routes/index';

//settings
app.set('port', process.env.PORT || 8000);

//middlewares
//app usa el modulo morgan en la versión de desarrollo
app.use(morgan('dev'));

//entender los datos que me envian con estructura Json
app.use(expres.json());

//cors options

const corsOptions = {
    "origin": "http://localhost:4200",
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE"
};

app.use(cors(corsOptions));

//routes
app.use('/api', indexRoutes);

//esta carpeta se utiliza para almacenar archivos publicos
app.use('./uploads', expres.static(path.resolve('uploads')));
//la funcion path.resolve(string) devuleve toda la ruta desde el inicio de una carpeta, ejemplo: ../c/user/unit/folfer/folder/.../uploads
//para no escribir todo esto, path.resolve(string) nos devuelve esa url

export default app;