import file from '../models/file';
import path from 'path';


export async function createDocument(document: any, page: string, d: Date, ispath: boolean): Promise<{}> {
    
    return new Promise<{}>((resolve, reject) =>{
        const newFile = {
            title: document.filename,
            date: `${d.getFullYear()}/${d.getMonth()<10 ? '0' + d.getMonth(): d.getMonth()}/${d.getDate()<10 ? 0 + d.getDate(): d.getDate()}-${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`,
            filePath: document.path,
            type: ispath ?path.extname(document.path).split('.')[1] : document.type,
            page: page
       }
        resolve(newFile);
    });
}

// function createDocument(docuemnte: {}, page: string, d:Date): Promise<{}>{

// }

export async function saveDocument(newFile:{}): Promise<{}> {
    const fl = new file(newFile);
    let json: {};
    try {
        await fl.save();
        json = ({
            message: 'la ruta del archivo se guardó con exito en la base de datos',
            error: '',
            fl
        });
    } catch (err) {
        console.log(err);
        json = ({
            message: '',
            error: 'Hubo un error al guardar',
        });
    }
    return json;
}