import {connect} from 'mongoose';

export async function starConnection() {
        connect('mongodb://localhost/booksdb', {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false
        }).catch(err => {
            console.log('error en conexion: ' + err);
        });   

    console.log('database is connected');
}