import {Router} from 'express';
const router = Router();

//controllers
import {getFiles,postFile, getFilesPage, deleteFiles, updateFile} from '../controllers/file.controller';
import { postUrlFile, updateUrlFile, deleteUrlFiles } from '../controllers/url.controller';

//libs
import multer from '../libs/multer'

router.route('/files')
    //file, es el fieldnmae que trae el request
    .post(multer.array('file',15), postFile)
    .get(getFiles)
    .delete(deleteFiles)
    .put(updateFile);
   
router.route('/:page')
    .get(getFilesPage);

router.route('/uploadppt')
    .post(postUrlFile)
    .put(updateUrlFile)
    .delete(deleteUrlFiles);


export default router;