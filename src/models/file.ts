import {Schema, model, Document} from 'mongoose';

const schema = new Schema({
    title: String,
    date: String,
    filePath: String,
    type: String,
    page: String,
});

//Interface que hereda de Document desde mongodb

interface IFile extends Document {
    title: string;
    date: string;
    filePath: string;
    type: string;
    page: string;
}

//create colecction file(s) into mongodb. en plural
export default model<IFile>('file', schema);